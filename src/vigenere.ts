export class Vigenere { 

    private ALPHA_LANG = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
    private FRENCH_LANG_FREQS = [9.42,1.02,2.64,3.39,15.87,0.95,1.04,0.77,8.41,0.89,0.00,5.34,3.24,7.15,5.14,2.86,1.06,6.46,7.90,7.26,6.24,2.15,0.00,0.30,0.24,0.32]
    
    public cipher(text: string, key: string, direction: number = +1){
        let currKeyIndex = 0;
        const a = text.split('').map((it, index) => {
            if(!it.match(/^[A-Za-z]$/)){
                return it 
            }
            const char = this.decaleChar(it, this.convertCharToDecalage(key[currKeyIndex]) * direction)
            currKeyIndex = (currKeyIndex + 1) % key.length
            return char
        })
        return a.join('')
    }

    public decipher(text: string, key: string){
        return this.cipher(text, key, -1)
    }

    public findKeySize(text: string, lang: string = 'fr') {
        const wrkingTxt = text.replace(/[^A-Za-z]/g, '')
        const possibleRepetitiveFragmentsSize = Array.from(new Array(Math.ceil(wrkingTxt.length/2)).keys()).slice(3)
        const a = possibleRepetitiveFragmentsSize.map((it) => {
            return wrkingTxt.split('').map((_, idx) => {
                return wrkingTxt.slice(idx, idx + it)
            })
        })
        const b = a.flat().filter((it) => it.length >= 3)
        const c = Object.fromEntries(b.map((it) => {
            let match 
            let indexes = []
            const re = new RegExp(it, 'g') 
            while ((match = re.exec(wrkingTxt)) != null) {
                indexes.push(match.index);
            }
            return [it, indexes]
        }).filter((it) => {
            return it[1].length == 2
        }).map((it: any) => {
            return [it[0], it[1][1] - it[1][0]]
        }))
        const d = [...new Set(Object.values(c))]
        const e = d.map((it: any) => {
            return Array.from(new Array(Math.floor(it/2))).map((_, idx) => {
                return idx + 2
            }).filter((jt: number) => {
                return it % jt === 0
            })
        })
        const f2 = (Object.entries(e.flat().reduce((acc: any, curr: number) => {
            if(acc[curr] == null) { acc[curr] = 0 }
            acc[curr]++
            return acc
        }, {})).sort((a:any, b:any) => a[1] >= b[1] ? -1 : 1))[0][0]
        return eval(f2)
    }

    private getTextFreqs(text: string): number[] {
        const a : Array<string> = text.replace(/[a-zA-Z]/g, (i) => i.toUpperCase()).match(/[A-Z]/g) ||[]
        const b =  (Object.fromEntries(Object.entries(a.reduce((acc: any, curr: any) => {
            if(acc[curr] == null) { acc[curr] = 0 }
            acc[curr]++
            acc.SUM++
            return acc;
        }, {SUM: 0}))))
        return Object.values(this.ALPHA_LANG.map(it => b[it] || 0 )) as number[]
    
    }

    private calculateIC(frequencies: number[]): number {
        let sumOfProducts = 0
        let sumOfFrequencies = 0

        for (const letter in frequencies) {
            const frequency = frequencies[letter];
            sumOfProducts += frequency * (frequency - 1);
          }

        for (const letter in frequencies) {
            const frequency = frequencies[letter];
            sumOfFrequencies += frequency;
          }

          return sumOfProducts / (sumOfFrequencies * (sumOfFrequencies - 1));


    }

    public findKey(text: string, lang: string = 'fr') {
        const size = this.findKeySize(text)
        const cleanText = (text.match(/[a-zA-Z]/g) || []).join('');

        const sectionnedTexts = Array.from(new Array(size)).map((_, it) => {
            return cleanText.split("").filter((_, i) => (i - it + size) % size == 0).join("")
        })
        console.log(cleanText)
        console.log(sectionnedTexts)
        const key = sectionnedTexts.map((it, idx) => {

            
            const f = Object.fromEntries(this.getTextFreqs(it).map((jt) => jt * 100 / it.length).map((jt, j) => [this.ALPHA_LANG[j], jt]))
            const rf = Object.fromEntries(this.ALPHA_LANG.map((jt, j) => [jt, this.FRENCH_LANG_FREQS[j]]))
            const mostFrequentInSection = Object.entries(f).sort((a,b) => a[1] > b[1] ? -1 : 1)[0][0]
            const mostFrequentInRef = Object.entries(rf).sort((a,b) => a[1] > b[1] ? -1 : 1)[0][0]
            const d = (mostFrequentInSection.charCodeAt(0) - mostFrequentInRef.charCodeAt(0) + this.ALPHA_LANG.length) % this.ALPHA_LANG.length
            const k = this.ALPHA_LANG[d]
            
            
            console.log(d, k, mostFrequentInSection, mostFrequentInRef, Object.entries(f).sort((a,b) => a[1] > b[1] ? -1 : 1) , Object.entries(rf).sort((a,b) => a[1] > b[1] ? -1 : 1) )
            return k
        }).join('')

        const d = this.decipher(text, key)
        
        console.log(key, d)
        return key
    }


    private decaleChar(char: string, decalage: number){
        let newCharCode = char.charCodeAt(0) + decalage
        if (char.toLowerCase().charCodeAt(0) + decalage > "z".charCodeAt(0)){
            newCharCode -= 26
        }
        if (char.toLowerCase().charCodeAt(0) + decalage < "a".charCodeAt(0)){
            newCharCode += 26
        }        
        return String.fromCharCode(newCharCode)
    }

    private convertCharToDecalage(char: string){
        return (char.toLowerCase().charCodeAt(0) - ('a').charCodeAt(0))
    }


 

}