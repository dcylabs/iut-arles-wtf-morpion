import { Vigenere } from "./vigenere"

describe('vigenere', () => {

    it('should run exercice cipher test cases', () => {
        const vigenere = new Vigenere()
        expect(vigenere.cipher("AAA AAA AAA AAA AAA AAA", "ABCD")).toEqual("ABC DAB CDA BCD ABC DAB")
        expect(vigenere.cipher("CA VA ETRE TOUT NOIR", "PIERRE")).toEqual("RI ZR VXGM XFLX CWMI")
    })

    it('should run exercice decipher test cases', () => {
        const vigenere = new Vigenere()
        expect(vigenere.decipher("ABC DAB CDA BCD ABC DAB", "ABCD")).toEqual("AAA AAA AAA AAA AAA AAA")
        expect(vigenere.decipher("RI ZR VXGM XFLX CWMI", "PIERRE")).toEqual("CA VA ETRE TOUT NOIR")
    })

    it('should cryptanlyse', () => {
        const vigenere = new Vigenere()
        const sample1 = `Ni jk xyspjo h'sx tgqimx, pc Pesmsl mvcmipopjo e sx qyxxcky pyyv deariro hc xsgb, etog jo fmex bow yspcc rmsvydvc, delnmq ayc vi bowqyyq niq kmjow cd pc zsgdvysp qyrr ni ayyjoyp mvcwi kyyariro hc xsgb. P'cctcmi nbiqorro yl nmkyvnrmqwi qobsop gxzcbwc : ve doqcvpc khsvxc owr zpsc kpkrbo uso pc wejo h'cxzgbsl 20 %, kzcm py diro ir ve oeiso hc mssvisb fperc cxpsic ni lymp, delnmq ayc vi kkpc khsvxc k py diro kpsw-zvis kzcm py aycei ebmqo. Pcc nsfilspcc vccwcwfjorr k py pikopjo ebepro. Wy cmjrssoxro il fsj owr mepkgrovgcxgayc, kzcm wcc egviq vslqycc, irbsgdiq ox nymldycc, ir csl fsj cxydmmxrysvc nmr << or Qkmld-Iqzvgd >> iqd ylo szcipferssl pvcaycxxc or zyvb ni pyyro.`
        const sample2 = "KQOWEFVJPUJUUNUKGLMEKJINMWUXFQMKJBGWRLFNFGHUDWUUMBSVLPSNCMUEKQCTESWREEKOYSSIWCTUAXYOTAPXPLWPNTCGOJBGFQHTDWXIZAYGFFNSXCSEYNCTSSPNTUJNYTGGWZGRWUUNEJUUQEAPYMEKQHUIDUXFPGUYTSMTFFSHNUOCZGMRUWEYTRGKMEEDCTVRECFBDJQCUSWVBPNLGOYLSKMTEFVJJTWWMFMWPNMEMTMHRSPXFSSKFFSTNUOCZGMDOEOYEEKCPJRGPMURSKHFRSEIUEVGOYCWXIZAYGOSAANYDOEOYJLWUNHAMEBFELXYVLWNOJNSIOFRWUCCESWKVIDGMUCGOCRUWGNMAAFFVNSIUDEKQHCEUCPFCMPVSUDGAVEMNYMAMVLFMAOYFNTQCUAFVFJNXKLNEIWCWODCCULWRIFTWGMUSWOVMATNYBUHTCOCWFYTNMGYTQMKBBNLGFBTWOJFTWGNTEJKNEEDCLDHWTYYIDGMVRDGMPLSWGJLAGOEEKJOFEKUYTAANYTDWIYBNLNYNPWEBFNLFYNAJEBFR"
        
        expect(vigenere.findKeySize(sample1)).toEqual(3)
        expect(vigenere.findKey(sample1)).toEqual("KEY")
        
        expect(vigenere.findKeySize(sample2)).toEqual(5)
        expect(vigenere.findKey(sample2)).toEqual("SCUBA")

    }) 

})