import { Vigenere } from "./src/vigenere";

interface Person {
    age: number,
    name: string,
    say(): string
}

let mike = {
    age: 25, 
    name:"Mike", 
    say: function() { 
        return "My name is " + this.name + 
               " and I'm " + this.age + " years old!"
    }
}

function sayIt(person: Person) {
    return person.say();
}

const a = new Vigenere()

console.log(sayIt(mike))
